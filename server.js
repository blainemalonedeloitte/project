
// Dependencies
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var mongodb = require('mongodb');
var mongoose = require('mongoose');
var MongoClient = mongodb.MongoClient;

// MongoDB
//mongoose.connect('mongodb://localhost/rest_test');

var url = "mongodb://localhost/rest_test";

/*request('http://private-bc4ec-lorcanoconnor.apiary-mock.com/questions', function (error, response, body) {
        var defect = JSON.parse(body);
});*/

/*request('https://jira.atlassian.com//rest/api/2/search?jql=project=JRA', function (error, response, body) {

        var defect = JSON.parse(body);

        MongoClient.connect(url, function(err ,db) {
        	if (err) {
				console.log('Unable to connect... Mongo');
			} else {
				console.log('Connected to Mongo');
				var collection = db.collection('defects').insertOne(defect);
				console.log('defect added' + collection);
			}

  			db.close();

  });*/

// Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

//Routes
app.use('/api', require('./routes/api'));

app.post("/rest/defects", function(req, res) {



});

app.get('/', function(req, res){
	res.sendFile('./public/app/pages/index.html', {
		root: __dirname
	});
})

// Start server
app.listen(process.env.PORT || 3000);
console.log('API is running on port 3000');


