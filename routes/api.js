
var express = require('express');
var router = express.Router();

var Product = require('../models/product');
Product.methods(['get', 'post']);
Product.register(router, '/products');

var Defect = require('../models/defect');
Defect.methods(['get', 'post', 'delete']);
Defect.register(router, '/defects');

module.exports = router;