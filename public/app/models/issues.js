(function(app){
	'use strict';
	(function(models){

		var effortOptions = ['Unknown', 'High', 'Medium', 'Small'];
		var workaroundOptions = ['None', 'Manual', 'Technical', 'User', 'User Inconvenience'];
		var costOptions = ['Standard', 'Fixed Date', 'Standard', 'Intangible'];
		var systemOptions = ['Multiple', 'External', 'Internal', 'None'];
		var usersOptions = ['100', '75', '50', '25%'];
		var featureOptions = ['Key', 'Standard', 'Niche', 'None'];

		var issues;

		var getIssues = function(filtered) {
			if (!filtered) {
				return issues;
			}

			var filters = app.services.filter.getFilter();
			console.log('michael', filters);

			return issues.filter(function(issue) {
				if (filters.feature) {
					var featureLimit = getNumericValue(filters.feature, featureOptions);
					if (issue.featureValue < featureLimit) {
						return false;
					}
				}

				return true;
			});
		};

		var setIssues = function(data) {
			issues = data.map(function(item) {
				item.effortValue = getNumericValue(item['Effort to fix:'], effortOptions);
				item.workaroundValue = getNumericValue(item['Workaround available'], workaroundOptions);
				item.costValue = getNumericValue(item['Cost of Delay'], costOptions);
				item.systemValue = getNumericValue(item['Systems Impacted'], systemOptions);
				item.usersValue = getNumericValue(item['Number of users impacted'], usersOptions);
				item.featureValue = getNumericValue(item['Feature Impacted'], featureOptions);

				return item;
			});
		};

		var getNumericValue = function(val, array) {
			return array.indexOf(val) + 1;
		};

		models.issues = {
			get: getIssues,
			set: setIssues
		};

	})(app.models || (app.models = {}));
})(window.app || (window.app = {}));