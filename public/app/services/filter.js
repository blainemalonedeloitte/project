(function(app){
	'use strict';
	(function(services){

		var filter = {
			feature: null,
			users: null,
			cost: null,
			system: null,
			workaround: null
		};

		var filterStages = [
			{id: 1, required: []},
			{id: 2, required: ['feature'] },
			{id: 3, required: ['feature', 'users', 'cost']},
			{id: 4, required: ['feature', 'users', 'cost', 'system']},
			{id: 5, required: ['feature', 'users', 'cost', 'system', 'workaround'], validSet: true},
		];

		function getFilterStage(){
			return filterStages.filter(function(s) {
				return s.required.every(function(p){
					return filter.hasOwnProperty(p) && filter[p];
				});
			})
			.slice(-1)[0];
		}

		services.filter = {
			getFilter: function() {return filter;},
			filterStage: function() { return getFilterStage().id;},
			isValid: function() { return !!getFilterStage().validSet;}
		};

	})(app.services || (app.services = {}));
})(window.app || (window.app = {}));