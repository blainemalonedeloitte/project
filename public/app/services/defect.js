(function(app){
	'use strict';
	(function(services) {

		var http = services.http;
		if (!http){
			throw new Error('must load http before defect service');
		}

		function getDefects(success, error){
			http.get('http://private-bc4ec-lorcanoconnor.apiary-mock.com/questions', success, error);
		}

		services.defect = {
			get: getDefects
		};

	})(app.services || (app.services = {}));
})(window.app || (window.app = {}));