(function(app){
	'use strict';
	(function(services){

		function getResponse(callback, error){
			return function() {
				if (this.readyState == 4){
					callback(JSON.parse(this.responseText), this.status);
				} else {
					error(this.responseText, this.status);
				}
			}
		}

		function getError(callback){
			return function() {
				callback(this.responseText, this.status);
			}
		}

		function httpGet(url, success, error) {
			var xhr = new XMLHttpRequest();
			xhr.addEventListener("load", getResponse(success, error));
			xhr.addEventListener("error", getError(error));
			xhr.addEventListener("abort", getError(error));
			xhr.open("GET", url, true);
			xhr.send();
		}

		function httpPost(url, body, success, error) {
			var xhr = new XMLHttpRequest();
			xhr.addEventListener("load", getResponse(success, error));
			xhr.addEventListener("error", getError(error));
			xhr.addEventListener("abort", getError(error));
			xhr.open("POST", url, true);
			xhr.send(body);
		}

		services.http = {
			get: httpGet,
			post: httpPost
		};

	})(app.services || (app.services = {}))
})(window.app || (window.app = {}));