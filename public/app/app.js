(function(app){
	var services = app.services;
	var models = app.models;
	var controllers = app.controllers;

	/**
	 * Fetch the latest issue data from Jira.
	 */
	var fetchIssues = function() {
		var defectService = services.defect;

		defectService.get(function(message, status){
			
			models.issues.set(message);

			var issues = models.issues.get(true);
			drawIssues(issues);
			controllers.chartCtrl.render(issues);

		}, function(message, status){
			console.log('error');
			console.log(message);
			console.log(status);
		});
	};

	/**
	 * Redraw the full issue list on the page.
	 */
	var drawIssues = function(data) {
		var count = document.getElementById('defect-count');
		count.innerHTML = '(' + data.length + ')';

		var ele = document.getElementById('issue-list');

		// Clear existing data
		while (ele.firstChild) {
		    ele.removeChild(ele.firstChild);
		}

		ele.appendChild(controllers.issueListCtrl.render(data));
	};

	/**
	 * App Initialisation
	 */
	return function() {
		app.fetchIssues = fetchIssues;
		fetchIssues();
	};
})(window.app)();