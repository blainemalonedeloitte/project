function onShowChartClick() {
  // Call the API for the new data.
  app.fetchIssues();
}


/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function showDropdown(dropdownId) {
    document.getElementById(dropdownId).classList.toggle("show");
}

function displayNext(area, value){
   var filterService = window.app.services.filter;
   filterService.getFilter()[area] = value;
   console.log(filterService.getFilter());
   
   var stage = filterService.filterStage();
   for(var i = 0; i < stage; ++i) {
    var sentence = document.getElementById("sentence-" + i);
    if (sentence) {
      sentence.classList.remove("hidden");
    }
   }

   document.getElementById(area + "Button").innerHTML = value;
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
