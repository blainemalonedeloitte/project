(function(app){
	'use strict';
	(function(controllers){

		var renderIssueList = function(issues) {
			var frag = document.createDocumentFragment();
			var ul = document.createElement('ul');

			issues.forEach(function(issue) {
				ul.appendChild(getIssueElement(issue));
			});

			frag.appendChild(ul);

			return frag;
		};

		var getIssueElement = function(issue) {
			var li = document.createElement('li');
			li.innerHTML = '<div class="panel panel-default"><div class="panel-heading">' + issue['Feature Impacted'] + ' Feature - ' + issue.Key + '</div><div class="panel-body">' + issue.Title + '</div></div>';
			return li;
		};

		controllers.issueListCtrl = {
			render: renderIssueList,
		};

	})(app.controllers || (app.controllers = {}));
})(window.app || (window.app = {}));