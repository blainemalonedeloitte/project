(function(app){
	'use strict';
	(function(controllers){

		var myRadarChart;

		var renderChart = function(issues) {
			if (myRadarChart) {
				myRadarChart.destroy();
			}

			var radarEle = document.getElementById("radar-chart");
			radarEle.innerHTML = '';

			var ctx = radarEle.getContext("2d");

			var datasets = issues.map(function(issue) {
				var points = [
					issue.effortValue,
					issue.workaroundValue,
					issue.costValue,
					issue.systemValue,
					issue.usersValue,
					issue.featureValue
				];

				// Color based on the severity.
				var total = points.reduce(function(prev, current) {
					return prev + current;
				});

				var percent = total / (points.length * 4);
				var colour = Math.floor(256 * percent);

				return {
					label: issue.Title,
					fillColor: 'rgba(' + colour + ',' + colour + ',' + colour + ',0.2)',
					strokeColor: 'rgba(' + colour + ',' + colour + ',' + colour + ',1)',
					pointColor: 'rgba(' + colour + ',' + colour + ',' + colour + ',1)',
					pointStrokeColor: '#fff',
					pointHighlightFill: '#fff',
					pointHighlightStroke: 'rgba(' + colour + ',' + colour + ',' + colour + ',1)',
					data: points
				};
			});

			var data = {
				labels: [
					"Effort to fix",
					"Workaround available",
					"Cost of Delay",
					"Systems Impacted",
					"Number of users impacted",
					"Feature Impacted"
				],
				datasets: datasets
			};

			var options = {
				// String - Template string for single tooltips
				tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
				// String - Template string for multiple tooltips
				multiTooltipTemplate: "<%= value + ' %' %>",
			};

			//You can give tooltipTemplate a function, and format the tooltip as you wish:

			//tooltipTemplate: function(v) {return someFunction(v.value);}
			//multiTooltipTemplate: function(v) {return someOtherFunction(v.value);}
			//Those given 'v' arguments contain lots of information besides the 'value' property. You can put a 'debugger' inside that function and inspect those yourself.

			myRadarChart = new Chart(ctx).Radar(data,{});
		};

		controllers.chartCtrl = {
			render: renderChart,
		};

	})(app.controllers || (app.controllers = {}));
})(window.app || (window.app = {}));