
var restful = require('node-restful');
var mongoose = restful.mongoose;

var productSchema = new mongoose.Schema({
	name: String
});

module.exports = restful.model('Products', productSchema);