
var restful = require('node-restful');
var mongoose = restful.mongoose;

var defectSchema = new mongoose.Schema({
	issue: Number,
	feature: String,
	users: String,
	workaround: String,
	cost_of_delay: Number,
	system: String
});

module.exports = restful.model('Defects', defectSchema);